# EXTRA CREDIT
##############################################################
# Homework #3
# name: Paul Mannarino
# sbuid: 108060069
##############################################################
.text



##############################
# Part 1 FUNCTION
##############################
.macro saveRegisters()
	addi $sp, $sp, -36
	sw $ra, 32($sp)
	sw $s0, 28($sp)
	sw $s1, 24($sp)
	sw $s2, 20($sp)
	sw $s3, 16($sp)
	sw $s4, 12($sp)
	sw $s5, 8($sp)
	sw $s6, 4($sp)
	sw $s7, 0($sp)
.end_macro

.macro loadRegisters()
	lw $s7, 0($sp)
	lw $s6, 4($sp)
	lw $s5, 8($sp)
	lw $s4, 12($sp)
	lw $s3, 16($sp)
	lw $s2, 20($sp)
	lw $s1, 24($sp)
	lw $s0, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
.end_macro

.macro print_int (%x)
	addi $sp, $sp, -4
	sw $a0, 0($sp)
	li $v0, 1
	la $a0 %x
	syscall
	li $v0, 4
	la $a0, breakLi
	syscall
	lw $a0, 0($sp)
	addi $sp, $sp, 4
.end_macro


arrayFill:
	saveRegisters()
	move $s0, $a0	# array address
	move $s1, $a1	# filename
	move $s2, $a2	# max bytes
	li $s3, 0 	# bytes loaded count
	
	
	# open file
	li   $v0, 13       # system call for open file
  	move   $a0, $s1    # output file name
 	li   $a1, 0        # Open for writing (flags are 0: read, 1: write)
 	li   $a2, 0        # mode is ignored
 	syscall            # open a file (file descriptor returned in $v0)
 	move $s6, $v0      # save the file descriptor  	
 	move $a1, $s0		# buffer address

	readFile:
		beq $s3, $s2, endRead	#check if max bytes met
		li $v0, 14	# read from file
		move $a0, $s6	# file descriptor
		li $a2, 1	# max bytes to read at once
		syscall		#read
		beqz $v0, endRead	# end of file, end
		bltz $v0, ioError	# less then 0, error
		addi $a1, $a1, 1	# increment buffer
		addi $s3, $s3, 1	# increment byte read count
		j readFile		#return to top loop
	ioError:
	li $v0, -1	# return -1 for error
	j arrayFill_end	# end program
	
	endRead:
	
	arrayFill_end:
	li $v0, 16	#close file
	move $a0, $s6	# get file descriptor
	syscall
	move $v0, $s3	# return bytes read
	loadRegisters()	#reload old register values
	jr $ra

find2Byte:
	saveRegisters()
	move $s0, $a0	# Address to array to read from
	move $s1, $a1	# search value, what to search
	move $s2, $a2	# num rows
	move $s3, $a3	# num cols
	
	sw $s2, rows
	sw $s3, cols
	
	move $s5, $s0
	searchLoop:
		lh $t0, 0($s5)	# load 2 bytes into t0
		beqz $t0, byteNotFound
		#sll $t0, $t0, 24
		#srl $t0, $t0, 24
		beq $t0, $s1, foundByte
		addi $s5, $s5, 2
		j searchLoop
		
	byteNotFound:
	li $a0, -9
	li $v0, -1
	li $v1, -1
	j find2Byte_end
	
	foundByte:
		# li $t9, 0xaf2b
		# sh $t9, 0($s5)
		# j find2Byte_end
		li $t1, 2
		subu $t0, $s5, $s0
		div $t0, $t1
		mflo $t0
		div $t0, $s3
		mflo $v0
		mfhi $v1
	
	find2Byte_end:
	loadRegisters()
	jr $ra


##############################
# PART 2/3 FUNCTION
##############################

playGame:
	saveRegisters()
	lb $t0, 0($a3)
	beq $t0, 10, terminatePlayGame	# no input, terminate program
	beqz $t0, terminatePlayGame	# no input, terminate program
	move $s0, $a0  	# Buffer address
	move $s1, $a1	# row of mower
	move $s2, $a2	# column of mower
	move $s3, $a3	# move string
	beq $s1, -1, firstTimeCheck	# validate position
	beq $s2, -1, firstTimeCheck	# validate position
	
	bltz $s1, terminatePlayGame
	bltz $s2, terminatePlayGame
	
	li $t0, 'n'
	sb $t0, firstTime
	
	j storeMowerPos
	
	firstTimeCheck:
		la $t0, firstTime
		lb $t1, 0($t0)
		beq $t1, 'n', movingOn
		
	beGentle:
		li $t1, 'n'
		sb $t1, 0($t0)
		li $t0, 24	# default row
		li $t1, 79	# default col
		sw $t0, start_r	# store default
		sw $t1, start_c	# store default
		j movingOn
	
	storeMowerPos:
	sw $s1, start_r	# save passed in row
	sw $s2, start_c	# save passed in col
	
	movingOn:
	move $a0, $s0
	lw $a1, start_r	# get mem local of mower
	lw $a2, start_c	# get mem local of mower
	jal getMemLocale  # get mower address
	move $t0, $v0	# save returned address

#j skipThis
	lh $t1, 0($t0)	
	srl $t1, $t1, 8
	sll $t1, $t1, 8
	addi $t1, $t1, 0x2b
	sh $t1, 0($t0)
skipThis:
	moveLoop:
		li $v0, 32	# SLEEP
		li $a0, 500	# 500ms
		syscall
		lb $t0, 0($s3)
		beqz $t0, playGame_end
		beq $t0, 'w', moveUp
		beq $t0, 'a', moveLeft
		beq $t0, 's', moveDown
		beq $t0, 'd', moveRight
		addi $s3, $s3, 1
		j moveLoop
	
	moveUp:
		move $a0, $s0	# refesh buffer address
		#jal mowTheLawn
		# detect collision FIRST
		# detect edge boundary		
		# Collision detect
		lw $t0, start_r
		addi $t0, $t0, -1
		
		move $a0, $s0
		move $a1, $t0
		lw $a2, start_c
		li $v1, 0
		jal checkCollision
		bgtz $v0, endMove
		move $a0, $s0	# refesh buffer address
		jal mowTheLawn
		bgtz $v1, wrapUpNow
		j dontMowUp
		
		wrapUpNow:
		li $t1, 25
		sw $t1, start_r
		#########################
		
		# decremtent the row, MOVE UP
		
		dontMowUp:
		lw $s1, start_r	  # get current row
		addi $s1, $s1, -1 # subtract 1
		sw $s1, start_r   # store new value
	
		
	
		# get new mem locale of lawn mower
		move $a0, $s0
		jal updateMower
		j endMove
		
	moveDown:
		move $a0, $s0	# refesh buffer address
		#jal mowTheLawn
		# detect collision FIRST
		# detect edge boundary		
		# Collision detect
		lw $t0, start_r
		addi $t0, $t0, 1
		
		move $a0, $s0
		move $a1, $t0
		lw $a2, start_c
		li $v1, 0
		jal checkCollision
		bgtz $v0, endMove
		move $a0, $s0	# refesh buffer address
		jal mowTheLawn
		bgtz $v1, wrapDownNow
		j dontMowDown
		
		wrapDownNow:
		li $t1, -1
		sw $t1, start_r
		#########################
		
		# decremtent the row, MOVE UP
		
		dontMowDown:
		lw $s1, start_r	  # get current row
		addi $s1, $s1, 1 # subtract 1
		sw $s1, start_r   # store new value
	
		
	
		# get new mem locale of lawn mower
		move $a0, $s0
		jal updateMower
		j endMove
		
	moveLeft:
		move $a0, $s0	# refesh buffer address
		#jal mowTheLawn
		# detect collision FIRST
		# detect edge boundary		
		# Collision detect
		lw $t0, start_c
		addi $t0, $t0, -1
		
		move $a0, $s0
		lw $a1, start_r
		move $a2, $t0
		li $v1, 0
		jal checkCollision
		bgtz $v0, endMove
		move $a0, $s0	# refesh buffer address
		jal mowTheLawn
		bgtz $v1, wrapLeftNow
		j dontMowLeft
		
		wrapLeftNow:
		li $t1, 80
		sw $t1, start_c
		#########################
		
		# decremtent the row, MOVE UP
		
		dontMowLeft:
		lw $s1, start_c	  # get current row
		addi $s1, $s1, -1 # subtract 1
		sw $s1, start_c   # store new value
		
		
	
		# get new mem locale of lawn mower
		move $a0, $s0
		jal updateMower
		j endMove
		
	moveRight:
		move $a0, $s0	# refesh buffer address
		#jal mowTheLawn
		# detect collision FIRST
		# detect edge boundary		
		# Collision detect
		lw $t0, start_c
		addi $t0, $t0, 1
		
		move $a0, $s0
		lw $a1, start_r
		move $a2, $t0
		li $v1, 0
		jal checkCollision
		bgtz $v0, endMove
		move $a0, $s0	# refesh buffer address
		jal mowTheLawn
		bgtz $v1, wrapRightNow
		j dontMowRight
		
		wrapRightNow:
		li $t1, -1
		sw $t1, start_c
		#########################
		
		# decremtent the row, MOVE UP
		
		dontMowRight:
		lw $s1, start_c	  # get current row
		addi $s1, $s1, 1 # subtract 1
		sw $s1, start_c   # store new value
		#jal mowTheLawn
		
	
		# get new mem locale of lawn mower
		move $a0, $s0
		jal updateMower
		j endMove

	endMove:
		addi $s3, $s3, 1	# incrment input
		j moveLoop
	wtf:
	li $v0, 4
	la $a0, why
	syscall
	
	playGame_end:
	loadRegisters()
	jr $ra
	
	terminatePlayGame:
	loadRegisters()
	li $v0, 10
	syscall
	
checkCollision:
	saveRegisters()
	# $a0 = buffer address
	# $a1 = temp row
	# $a2 = temp col

	bltz $a1, wrapUp
	bltz $a2, wrapLeft
	bgt $a1, 24, wrapDown
	bgt $a2, 79, wrapRight
	j noWrap
	
	wrapUp:
		li $a1, 24
		li $v1, 1
		j noWrap
	wrapDown:
		li $a1, 0
		li $v1, 1
		j noWrap
	wrapLeft:
		li $a2, 79
		li $v1, 1
		j noWrap
	wrapRight:
		li $a2, 0
		li $v1, 1
		j noWrap
		
	noWrap:
	jal getMemLocale
	move $t0, $v0	 # retrieve mem locale
	lhu $t1, 0($t0)
	li $t2, 0xb06f
	
	beq $t1, 0x4f20, collision	# water
	beq $t1, 0x3f20, collision	# dirt
	beq $t1, 0x7f52, collision	# rock
	beq $t1, 0x2f5e, collision	# tree
	beq $t1, $t2, collision		# flower
	li $v0, 0

	j check_end
	collision:
		li $v0, 1
	check_end:
		loadRegisters()
		jr $ra

mowTheLawn:
	saveRegisters()
	lw $a1, start_r	# get mower row
	lw $a2, start_c # get mower col
	jal getMemLocale # get mower mem locale
	move $t0, $v0	 # retrieve mem locale	
	# mow the spot, change to empty space
	lh $t1, 0($t0)	# get mower local
	srl $t1, $t1, 8	# clear char byte
	sll $t1, $t1, 8	# clear char byte
	addi $t1, $t1, 0x20
	ori $t1, $t1, 0x8000
	sh $t1, 0($t0)	    # store value	
	
	loadRegisters()
	jr $ra
	
updateMower:
	saveRegisters()
	# get new mem locale of lawn mower
	lw $a1, start_r
	lw $a2, start_c
	jal getMemLocale
	move $t0, $v0
	###################################
	
	# put the mower on the current spot
	lh $t1, 0($t0)		# get lawn mower position
	srl $t1, $t1, 8		# clear char byte
	sll $t1, $t1, 8		# clear char byte
	addi $t1, $t1, 0x2b	# add ' '
	#ori $t1, $t1, 0x8000
	sh $t1, 0($t0)		# store mowed space with '+' on it
	
	loadRegisters()
	jr $ra
	
getMemLocale:
	saveRegisters()
	move $s0, $a0 # base address
	move $s1, $a1 # current x
	move $s2, $a2 # current y
	lw $t1, cols	# num cols
	li $t2, 2	# data size
	mul $t0, $s1, $t1 # x * numCols
	add $t0, $t0, $s2 # (x * numCols) + y
	mul $t0, $t0, $t2 # ((x * numCols) + y ) * 2
	add $v0, $t0, $s0 # (((x * numCols) + y ) * 2) + baseAddress
	move $a0, $v0
	loadRegisters()
	jr $ra	


extraCreditLogo:
	saveRegisters()
	li $s0, 0xffff0000
	la $s1, logo
	extraLoop:
		lh $t0, 0($s1)
		beqz $t0, extraLoop_end
		sh $t0, 0($s0)
		addi $s0, $s0, 2
		addi $s1, $s1, 2
		j extraLoop
	extraLoop_end:
		loadRegisters()
		jr $ra
	

.data
rows: 	.word 4
cols:	.word 4
breakLi: .asciiz "\n"
mowPos: .word 4
start_r: .word 4
start_c: .word 4
why: .asciiz "wtf"
firstTime: .asciiz "y"
logo: .asciiz "O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_ / / / /C/C/C/C/C/C/C/C/C/ / / / / / / / / /S/S/S/S/S/S/S/S/S/ / / / / / / / / /E/E/E/E/E/E/E/E/E/ / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / ? ? ? ?C?C? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?S?S? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?E?E? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? O O O OCOCO O O O O O O O O O O O O O O O OSOSOSOSOSOSOSOSO O O O O O O O O O OEOEOEOEOEOEOEO O O O O O O O O O O O O O O O O O O O O O O O O O O O O O O O O O _ _ _ _C_C_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _S_S_ _ _ _ _ _ _ _ _ _ _E_E_E_E_E_E_E_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ o o o oCoCo o o o o o o o o o o o o o o o o o o o o o oSoSo o o o o o o o o o oEoEo o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o 0 0 0 0C0C0C0C0C0C0C0C0C0 0 0 0 0 0 0 0 0S0S0S0S0S0S0S0S0S0 0 0 0 0 0 0 0 0 0 0E0E0E0E0E0E0E0E0E0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_ / / / /2/2/2/2/2/2/2/2/2/ / / / / / / / / /2/2/2/2/2/2/2/2/2/ / / / / / / / / /0/0/0/0/0/0/0/0/0/ / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / ? ? ? ? ? ? ? ? ? ? ?2?2? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?2?2? ? ? ? ? ? ? ? ? ?0?0? ? ? ? ? ?0?0? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? O O O O O O O O O O O2O2O O O O O O O O O O O O O O O O O2O2O O O O O O O O O O0O0O O O O O O0O0O O O O O O O O O O O O O O O O O O O O O O O O O O O O O O O O _ _ _ _ _2_2_2_2_2_2_2_2_ _ _ _ _ _ _ _ _ _ _2_2_2_2_2_2_2_2_ _ _ _ _ _ _ _ _ _0_0_ _ _ _ _ _0_0_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ o o o o o2o2o o o o o o o o o o o o o o o o o2o2o o o o o o o o o o o o o o o o0o0o o o o o o0o0o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o 0 0 0 0 02020202020202020 0 0 0 0 0 0 0 0 0 0202020202020202020 0 0 0 0 0 0 0 0000000000000000000 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_ / / / / / / / / / /Y/ / / / / /Y/ / / / / / / / / / / / / /A/ / / / / / / / / / / / / /Y/ / / / / /Y/ / / /!/ / / / / / / / / / / / / / / / / / / / / / / / / / ? ? ? ? ? ? ? ? ? ?Y? ? ? ?Y? ? ? ? ? ? ? ? ? ? ? ? ? ?A? ?A? ? ? ? ? ? ? ? ? ? ? ? ? ?Y? ? ? ?Y? ? ? ? ?!? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? O O O O O O O O O OYO OYO O O O O O O O O O O O O OAOAOAOAOAO O O O O O O O O O O O O OYO OYO O O O O O!O O O O O O O O O O O O O O O O O O O O O O O O O O O O _ _ _ _ _ _ _ _ _ _Y_ _ _ _ _ _ _ _ _ _ _ _ _ _A_ _ _ _ _ _A_ _ _ _ _ _ _ _ _ _ _ _ _ _Y_ _ _ _ _ _ _!_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ o o o o o o o o oYo o o o o o o o o o o o oAo o o o o o o oAo o o o o o o o o o o o oYo o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o o 0 0 0 0 0 0 0 0Y0 0 0 0 0 0 0 0 0 0 0 0A0 0 0 0 0 0 0 0 0 0A0 0 0 0 0 0 0 0 0 0 0 0Y0 0 0 0 0 0 0!0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_O_"
