.data 

filenameBuffer: .space 80
filename: .space 20
	  .byte '\0'	
endFileMsg: .asciiz "\nEnd of file\n"
filePrompt:	.asciiz "\nPlease enter filename: "
sequencePrompt:	.asciiz "\nPlease enter move sequence: "
breakL:	.asciiz "\n"
inputBuffer: .asciiz "w"
inbuff: .space 80
playball: .asciiz "Moves: "
.text


.macro print_int (%x)
	addi $sp, $sp, -4
	sw $a0, 0($sp)
	li $v0, 1
	move $a0, %x
	syscall
	li $v0, 4
	la $a0, breakLi
	syscall
	lw $a0, 0($sp)
	addi $sp, $sp, 4
.end_macro


.globl main

main:
#jal extraCreditLogo
	# GET FILENAME FROM USER
	li, $v0, 4	#user prompt
	la $a0, filePrompt
	syscall		
	li $v0, 8	#collect user input
	la $a0, filenameBuffer
	li $a1, 80
	syscall
	
	jal cleanString	
	
	li $v0, 4
	syscall
	
	arrayFillTest:
	# ARRAY-FILL
	li $a0 0xffff0000
	la $a1, filename
	li $a2, 4000
	jal arrayFill
	
	#FIND-2-BYTE
	li $a0 0xffff0000
	li $a1, 0x2f2b
	li $a2, 25
	li $a3, 80
	jal find2Byte

	
	li $a0, 0xffff0000
	move $a1, $v0
	move $a2, $v1
	la $a3, inputBuffer
	jal playGame
	#j endMain
	
	
	letsPlay:
	li $v0, 4
	la $a0, playball
	syscall
	li $v0, 8	#collect user input
	la $a0, inbuff
	li $a1, 20
	syscall
	
	li $a0, 0xffff0000
	li $a1, -1
	li $a2, -1
	la $a3, inbuff
	jal playGame
	j letsPlay
	
	
	
	endMain:
	li $v0, 10
	syscall
	
cleanString:
	move $t0, $a0
	la $t1, filename
	cleanLoop:
		lb $t2, 0($t0)
		beq $t2, 0xa, endClean
		sb $t2, 0($t1)
		addi $t0, $t0, 1
		addi $t1, $t1, 1
		j cleanLoop
	endClean:
	jr $ra
	


.include "hw3.asm"
