+
#Homeowork #1
#name: Paul Mannarino
#sbuid: 108060069

.text


.globl _main
_main:

	li, $v0, 4	#user prompt
	la $a0, prompt1	#
	syscall		#
	

	li $v0, 5	#collect user input
	syscall		#
	move $t0, $v0	# move it to register $t0
	
	bltz $v0, if_neg
	
	#Will continue if positive
	
	li $v0, 4	 #2 comp label
	la $a0, two_comp #
	syscall		 #

	
	li $v0, 1	#show orgnl value
	move $a0, $t0	#
	syscall		#
	
	jal padding
	
	li $v0, 34	#show hex value
	move $a0, $t0	#
	syscall		#
	
	jal padding
	
	#Convert to 2's comp
	la $t1, 0	   #$t1 = 0
	add $a0, $t0, $t1  # org_val + 0
	li $v0, 35	   #sys call for binary
	syscall		   #
	
	jal padding
	
	li $v0, 1	#show org_value
	move $a0, $t0 	#
	syscall		#
	
	###########
	jal newLine
	###########
	
	li $v0, 4	  #show 1 comp label
	la $a0, one_comp  #
	syscall		  #
	
	li $v0, 1	#display org_value
	move $a0, $t0   #
	syscall		#
	
	jal padding
	
	li $v0, 34	#show hex value
	move $a0, $t0
	syscall		#
	
	jal padding
	
	li $v0, 35	#show binary value
	move $a0, $t0	#
	syscall		#
	

	jal padding

	li $v0, 1	#show orgnl_value
	move $a0, $t0	#
	syscall		#

	###########
	jal newLine
	###########
	
	li, $v0, 4	#show neg_one label
	la $a0, neg_one	#
	syscall		#
	
	#NEGATE orgnl_value
	la $t2, -1	   #$t2 = -1
	mul $t1, $t0, $t2  #$t1 = $t0*(-1)

	li $v0, 1	#print negated value
	move $a0, $t1	#
	syscall		#
	
	jal padding
	
	li $v0, 34	   #Display hex value
	la $t2, 1	   #
	sub $a0, $t1, $t2  #$a0 = $t1 - 1
	syscall		   #
	
	jal padding
	
	li $v0, 35	   #load binary syscall
	xor $t4, $t1, $t2  #FLIP BITS: $t4 = $t0 xor (-1) 
	move $a0, $t4	   #
	syscall		   #
	
	jal padding
	
	li $v0, 1	   #PRINT BINARY
	addi $a0, $t1, -1  # $a0 = $t1 - 1
	syscall		   #
	
	###########
	jal newLine
	###########
	
	li $v0, 4	 #neg_sing label
	la $a0, neg_sign #
	syscall 	 #
	
	li $v0, 1	# print negates org_val
	move $a0, $t1	# $t0 = $t1(-1)
	syscall		#
	
	jal padding
	
	li $v0, 34	   #print hex value
	la $t7, 2147483648 #load binary val 100000..0 to flip sign bit
	xor $t6, $t0, $t7  #flip org_val w/100000..0
	move $a0, $t6	   #
	syscall	  	   #
	
	jal padding
	
	li $v0, 35	#print negated binary num
	move $a0, $t6	#
	syscall		#
	
	jal padding
	
	li $v0, 1	#print binary val as decimal
	move $a0, $t6	#
	syscall		#
	
	###########
	jal newLine
	###########
	
	main_return:
	jal ieee_exp
	
	
	#End program
	li $v0, 10
	syscall
	
	#If the value is negative
if_neg:
	li $v0, 4
	la $a0, two_comp
	syscall

	li $v0, 1
	move $a0, $t0
	syscall

	jal padding

	la $t6, 1
	la $t7, -1   #trying to flip bits of neg num, then i have to add 1!!
	xor $t1, $t0, $t7
	#add $t1, $t1, $t6
	li $v0, 34
	move $a0, $t0
	syscall
	
	jal padding
	
	li $v0, 35
	move $a0, $t0
	syscall
	
	jal padding

	li $v0, 1
	move $a0, $t0
	syscall
	jal newLine		
	# VV one comp VV #
	li $v0, 4
	la $a0, one_comp
	syscall
	
	li $v0, 1
	move $a0, $t0
	syscall
	jal padding
	
	li $v0, 34
	add $t1, $t0, $t7
	move $a0, $t1
	syscall
	jal padding
	
	li $v0, 35
	move $a0, $t1
	syscall
	jal padding
	
	li $v0, 1
	move $a0, $t1
	syscall 
	
	###########
	jal newLine
	###########
	# VV new one comp VV #
	li $v0, 4
	la $a0, neg_one
	syscall
	
	li $v0, 1
	#xor $t1, $t0, $t7
	mul $t1, $t0, $t7
	li $v0, 1
	move $a0, $t1
	syscall
	jal padding
	li $v0, 34
	move $a0, $t1
	syscall
	jal padding
	li $v0, 35
	move $a0, $t1
	syscall
	jal padding
	li $v0, 1
	move $a0, $t1
	syscall
	
	###########
	jal newLine
	###########
	
	# VV neg signed VV #
	li $v0, 4
	la $a0, neg_sign
	syscall
	
	li $v0, 1
	move $a0, $t1
	syscall
	jal padding
	li $v0, 34
	move $a0, $t1
	syscall
	jal padding
	li $v0, 35
	move $a0, $t1
	syscall	
	jal padding
	li $v0, 1
	move $a0, $t1
	syscall
	
	###########
	jal newLine
	###########
	###########
	jal newLine
	###########
	
	j main_return
		
		
	ieee_exp:	#gets the exponent value from the number in $t0
		#addi $sp, $sp, -4
		#sw $s0, 0($sp)
		
		li $v0, 1
		move $a0, $t0
		syscall
		jal newLine
		
		li $v0, 4
		la $a0, sng_prsn
		syscall
		
		sll $t5, $t0, 1
		srl $t5, $t6, 24
		addi $t7, $t5, -127
		li $v0, 1
		move $a0, $t5
		syscall
		jal padding
		li $v0, 1
		move $a0, $t7
		syscall
		jal padding
		
		#takes care of single precision#
		##Check inf##
		srl $t9, $t0, 31 #grab sign bit
		sll $k0, $t0, 9
		add $k0, $k0, $t5
		li $t8, 255
		beq $k0, $t8, sng_inf
		return1:
		
		##Check 0##
		jal padding
		beqz $k0, sng_zero
		return2:
		
		#takes care of double percision#
		#srl $t9, $t0, 63
		#li $t8, 
		
		
		jal newLine
		li $v0, 4
		la $a0, dbl_prsn
		syscall
		
		sll $t5, $t0, 1
		srl $t5, $t6, 21
		addi $t7, $t5, -1023
		li $v0, 1
		move $a0, $t5
		syscall
		jal padding
		li $v0, 1
		move $a0, $t7
		syscall
		
		li $v0, 10
		syscall
		
	sng_inf:
		slt $k0, $t9, $t6
		beqz $k0, else
		li $v0, 4
		la $a0, neg_inf
		syscall
		j return1
		else:
			li $v0, 4
			la $a0, pos_inf
			syscall
			j return1
			
		
	
	
	dbl_inf:
	
	
	sng_zero:
	li $v0, 4
	la $a0, confirm
	syscall
		beqz $t9, else2
		li $v0, 4
		la $a0, neg_zero_str
		syscall
		j return2
		else2:
			li $v0, 4
			la $a0, pos_zero_str
			syscall
			j return2
	
	
	dbl_zero:
	
		
	nan:
	
			
		
	padding:
		li $v0, 4
		la $a0, pad
		syscall
		jr $ra
	newLine:
		li $v0, 4
		la $a0, nl
		syscall
		jr $ra
	

		
	
.data
prompt1: .asciiz "Enter an integer number:\t"
two_comp: .asciiz "2's complement:       "
one_comp: .asciiz "1's complement:       "
neg_one:  .asciiz "Neg 1's complement:   "
neg_sign: .asciiz "Neg Signed Magnitude: "
sng_prsn: .asciiz "IEE-754 signle precision:    "
dbl_prsn: .asciiz "IEE-754 double precision:    "
pos_inf: .asciiz "+INF"
neg_inf: .asciiz "-INF"
pos_zero_str: .asciiz "+0"
neg_zero_str: .asciiz "-0"
nan_str: .asciiz "NAN" 
confirm: .asciiz "You made it!"
returned: .asciiz "Made it back"

pad: .asciiz "  "
nl: .asciiz "\n"
