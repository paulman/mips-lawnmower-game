##############################################################
# Homework #3
# name: Paul Mannarino
# sbuid: 108060069
##############################################################
.text



##############################
# Part 1 FUNCTION
##############################
.macro saveRegisters
	addi $sp, $sp, -36
	sw $ra, 32($sp)
	sw $s0, 28($sp)
	sw $s1, 24($sp)
	sw $s2, 20($sp)
	sw $s3, 16($sp)
	sw $s4, 12($sp)
	sw $s5, 8($sp)
	sw $s6, 4($sp)
	sw $s7, 0($sp)
.end_macro

.macro loadRegisters
	lw $s7, 0($sp)
	lw $s6, 4($sp)
	lw $s5, 8($sp)
	lw $s4, 12($sp)
	lw $s3, 16($sp)
	lw $s2, 20($sp)
	lw $s1, 24($sp)
	lw $s0, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
.end_macro

.macro print_int (%x)
	addi $sp, $sp, -4
	sw $a0, 0($sp)
	li $v0, 1
	add $a0, $zero, %x
	syscall
	li $v0, 4
	la $a0, breakLi
	syscall
	lw $a0, 0($sp)
	addi $sp, $sp, 4
.end_macro


arrayFill:
	saveRegisters
	move $s0, $a0	# array address
	move $s1, $a1	# filename
	move $s2, $a2	# max bytes
	li $s3, 0 	# bytes loaded count
	
	
	# open file
	li   $v0, 13       # system call for open file
  	move   $a0, $s1    # output file name
 	li   $a1, 0        # Open for writing (flags are 0: read, 1: write)
 	li   $a2, 0        # mode is ignored
 	syscall            # open a file (file descriptor returned in $v0)
 	move $s6, $v0      # save the file descriptor  	
 	move $a1, $s0		# buffer address

	readFile:
		beq $s3, $s2, endRead	#check if max bytes met
		li $v0, 14	# read from file
		move $a0, $s6	# file descriptor
		li $a2, 1	# max bytes to read at once
		syscall		#read
		beqz $v0, endRead	# end of file, end
		bltz $v0, ioError	# less then 0, error
		addi $a1, $a1, 1	# increment buffer
		addi $s3, $s3, 1	# increment byte read count
		j readFile		#return to top loop
	ioError:
	li $v0, -1	# return -1 for error
	j arrayFill_end	# end program
	
	endRead:
	
	arrayFill_end:
	li $v0, 16	#close file
	move $a0, $s6	# get file descriptor
	syscall
	move $v0, $s3	# return bytes read
	loadRegisters	#reload old register values
	jr $ra

find2Byte:
	saveRegisters
	move $s0, $a0	# Address to array to read from
	move $s1, $a1	# search value, what to search
	move $s2, $a2	# num rows
	move $s3, $a3	# num cols
	
	sw $s2, rows
	sw $s3, cols
	
	move $s5, $s0
	searchLoop:
		lh $t0, 0($s5)	# load 2 bytes into t0
		beqz $t0, byteNotFound
		sll $t0, $t0, 24
		srl $t0, $t0, 24
		beq $t0, $s1, foundByte
		addi $s5, $s5, 2
		j searchLoop
		
	byteNotFound:
	li $a0, -9
	li $v0, -1
	li $v1, -1
	j find2Byte_end
	
	foundByte:
		# li $t9, 0xaf2b
		# sh $t9, 0($s5)
		# j find2Byte_end
		li $t1, 2
		subu $t0, $s5, $s0
		div $t0, $t1
		mflo $t0
		div $t0, $s3
		mflo $v0
		mfhi $v1
	
	find2Byte_end:
	loadRegisters
	jr $ra


##############################
# PART 2/3 FUNCTION
##############################

playGame:
	saveRegisters
	move $s0, $a0  	# Buffer address
	move $s1, $a1	# row of mower
	move $s2, $a2	# column of mower
	move $s3, $a3	# move string
	bltz $s1, saveLawnMower	# validate position
	bltz $s2, saveLawnMower	# validate position
	j movingOn	# skip default save
	
	saveLawnMower:	# save default location
		li $t0, 24	# default row
		li $t1, 79	# default col
		sw $t0, start_r	# store default
		sw $t1, start_c	# store default
		j invalidPos	# resume function
	
	movingOn:
	sw $s1, start_r	# save passed in row
	sw $s2, start_c	# save passed in col
	invalidPos:
	move $a0, $s0
	lw $a1, start_r	# get mem local of mower
	lw $a2, start_c	# get mem local of mower
	jal getMemLocale  # get mower address
	move $t0, $v0	# save returned address
	lw $a0, start_r # 
	lw $a0, start_c
	#j playGame_end
	
	#lh $t9, 0xaf2b
	#sh $t9, 0($t0)
	lh $t1, 0($t0)	
	srl $t1, $t1, 8
	sll $t1, $t1, 8
	addi $t1, $t1, 0x2b
	ori $t1, $t1, 0x8000
	sh $t1, 0($t0)
	
	moveLoop:
		lb $t0, 0($s3)
		beqz $t0, playGame_end
		beq $t0, 'w', moveUp
		#beq $t0, 'a', moveLeft
		#beq $t0, 's', moveDown
		#beq $t0, 'd', moveRight
		addi $s3, $s3, 1
		j moveLoop
	
	moveUp:
		# detect collision FIRST
		# detect edge boundary
		#move $a0, $s0
		#lw $a1, rows
		#lw $a2, cols
		#jal findMower
		
		lw $a1, start_r
		lw $a2, start_c
		move $a0, $s0
		jal getMemLocale
		lh $t0, 0($v0)
		srl $t0, $t0, 8
		sll $t0, $t0, 8
		addi $t0, $t0, 0x20
		sh $t0, 0($v0)	
			
		addi $a1, $s1, -1
		sw $a1, start_r
		move $a2, $s2
		move $a0, $s0
		print_int($a0)
		jal getMemLocale
		move $t0, $v0
		lh $t1, 0($t0)
		ori $t1, $t1, 0x8000
		srl $t1, $t1, 8
		sll $t1, $t1, 8
		addi $t1, $t1, 0x2b
		sh $t1, 0($t0)
		addi $s3, $s3, 1
		j moveLoop
		
	#li $v0, 32	# SLEEP
	#li $a0, 500	# 500ms
	#syscall
	
	playGame_end:
	loadRegisters
	jr $ra

	
findMower:
	saveRegisters
	move $s1, $a1 	# move row of mower
	move $s2, $a2	# move col of mower
	
	li $a1, 0x2b	# find plus sign
	move $a2, $s1	# move
	move $a3, $s2	# move
	jal find2Byte
	move $a1, $v0	# get row value
	move $a2, $v1	# get col value
	jal getMemLocale # get mem locale of [row][col]
	# $vo holds value
	loadRegisters
	jr $ra
	
getMemLocale:
	saveRegisters
	move $s0, $a0 # base address
	move $s1, $a1 # current x
	move $s2, $a2 # current y
	lw $t1, cols	# num cols
	li $t2, 2	# data size
	mul $t0, $s1, $t1 # x * numCols
	add $t0, $t0, $s2 # (x * numCols) + y
	mul $t0, $t0, $t2 # ((x * numCols) + y ) * 2
	add $v0, $t0, $s0 # (((x * numCols) + y ) * 2) + baseAddress
	move $a0, $v0
	loadRegisters
	jr $ra	
##############################
# Collision detection.  only move if space = 0x202b
# beq $t0, 0x202b, mow

#mow:
#	sb 
##############################
collisionDetection:
	

.data
rows: 	.word 4
cols:	.word 4
breakLi: .asciiz "\n"
mowPos: .word 4
start_r: .word 4
start_c: .word 4
